# ds-docs

#### Description
DolphinScheduler(ds) 海豚调度部署、实施、使用等各种操作相关文档记录，本仓以 markdown 文档 + shell 脚本为主。
本仓文档包含 ds 基于 x86_64/arm64 两种架构，CentOS/Ubuntu/openEuler 三种操作系统。
大家如果有 ds 文档相关需求的话，欢迎提 issue 哟！

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
