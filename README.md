# ds-docs

#### 介绍
DolphinScheduler(ds) 海豚调度部署、实施、使用等各种操作相关文档记录，本仓以 markdown 文档 + shell 脚本为主。
本仓文档包含 ds 基于 x86_64/arm64 两种架构，CentOS/Ubuntu/openEuler 三种操作系统。
大家如果有 ds 文档相关需求的话，欢迎提 issue 哟！

#### 软件架构
软件架构说明


#### 安装教程

DolphinScheduler(DS) deployment and implementation document list

## 1. DS on CentOS

eg. CentOS 7.6

#### 1.1 x86_64(eg. amd64)

- host mode
  + a. Standalone
  + b. Pseudo distribution
  + c. Distributed cluster

- docker mode

#### 1.2 arm64(Armv8 kunpeng920)

- host mode
  + a. Standalone
  + b. Pseudo distribution
  + c. Distributed cluster

- docker mode

## 2. DS on Ubuntu

eg. Ubuntu 18.04 LTS

#### 2.1 x86_64

- host mode
  + a. Standalone
  + b. Pseudo distribution
  + c. Distributed cluster

- docker mode

#### 2.2 arm64

- host mode
  + a. Standalone
  + b. Pseudo distribution
  + c. Distributed cluster

- docker mode

## 3. DS on openEuler

eg. openEuler 20.03 (LTS-SP1)

#### 3.1 x86_64

- host mode
  + a. Standalone
  + b. Pseudo distribution
  + c. Distributed cluster

- docker mode

#### 3.2 arm64

- host mode
  + a. Standalone
  + b. Pseudo distribution
  + c. Distributed cluster

- docker mode

## 4. DS Operation case

eg. ds 1.3.9



#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
